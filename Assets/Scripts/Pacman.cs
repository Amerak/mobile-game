using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Pacman : MonoBehaviour
{

    public Movement movement;
    private Gyroscope gyroscope;
    private bool gyroEnabled;


    private void Awake()
    {
        this.movement = GetComponent<Movement>();
        gyroEnabled = EnableGyro();
    }

    void Update()
    {
        if (gyroEnabled) {
            Quaternion gyroOutput = gyroscope.attitude;
            if (gyroOutput.x > 0.2) {
                Debug.Log("left");
                this.movement.SetDirection(Vector2.left);
            }
            else if (gyroOutput.x < -0.2)
            {
                Debug.Log("right");
                this.movement.SetDirection(Vector2.right);
            }
            else if (gyroOutput.y > 0.2)
            {
                Debug.Log("down");
                this.movement.SetDirection(Vector2.down);
            }
            else if (gyroOutput.y < -0.2)
            {
                Debug.Log("up");
                this.movement.SetDirection(Vector2.up);
            }

            // Rotace pacmana
            float angle = Mathf.Atan2(this.movement.direction.y, this.movement.direction.x);
            this.transform.rotation = Quaternion.AngleAxis(angle * Mathf.Rad2Deg, Vector3.forward);
        }
    }

    private bool EnableGyro()
    {
        if (SystemInfo.supportsGyroscope)
        {
            gyroscope = Input.gyro;
            gyroscope.enabled = true;
            return true;
        }

        return false;
    }
}
