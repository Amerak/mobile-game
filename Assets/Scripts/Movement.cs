using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class Movement : MonoBehaviour
{
    public new Rigidbody2D rigidbody { get; private set; }
    public float speed = 8f;
    public float speedMultiplier = 1.0f;
    public Vector2 initialDirection;
    public LayerMask wallLayer;
    public Vector2 direction { get; private set; }
    public Vector2 nextDirection { get; private set; }
    public Vector3 startingPosition { get; private set; }

    private void Awake()
    {
        this.rigidbody = GetComponent<Rigidbody2D>();
        this.startingPosition = this.transform.position;
    }

    private void Start()
    {
        ResetState();
    }

    private void ResetState()
    {
        this.speedMultiplier = 1.0f;
        this.direction = this.initialDirection;
        this.nextDirection = Vector2.zero;
        this.transform.position = this.startingPosition;
        this.rigidbody.isKinematic = false;
        this.enabled = true;
    }

    private void Update()
    {
        if (this.nextDirection != Vector2.zero) 
        {
            SetDirection(this.nextDirection);
        }
    }

    private void FixedUpdate()
    {
        Debug.Log(Time.timeSinceLevelLoad); 
        Vector2 position = this.rigidbody.position;
        Vector2 translation = this.direction * this.speed * this.speedMultiplier * Time.fixedDeltaTime;

        this.rigidbody.MovePosition(position + translation);
    }

    public void SetDirection(Vector2 direction)
    {
        if (!Occupied(direction))
        {
            this.direction = direction;
            this.nextDirection = Vector2.zero;
        }
        else
        {
            this.nextDirection = direction;
        }
    }

    public bool Occupied(Vector2 direction)
    {
        // pozice, velikost objektu, rotace, sm�r, vzd�lenost, s ��m koliduje 
        RaycastHit2D hit = Physics2D.BoxCast(this.transform.position, Vector2.one * 0.4f, 0.0f, direction, 1.5f, this.wallLayer);
        return hit.collider != null;
    }

}
