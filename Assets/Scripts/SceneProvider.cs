using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class SceneProvider : MonoBehaviour
{
    public static void GoToGameScene() {
        SceneManager.LoadScene("Game");
    }

    public static void GoToSecondLevel()
    {
        SceneManager.LoadScene("Game2");
    }

    public static void GoToMenuScene()
    {
        SceneManager.LoadScene("Menu");
    }

    public static void GoToScoreboardScene()
    {
        SceneManager.LoadScene("Scoreboard");
    }
}
