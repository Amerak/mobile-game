using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ScoreProvider : MonoBehaviour
{
    private static int levelOneScore;
    private static int levelTwoScore;
    private static int levelOneTimeScore;
    private static int levelTwoTimeScore;

    public static void setScoreForLevel(int score, int level) {
        if (level == 1)
        {
            levelOneScore = score;
        }
        else levelTwoScore = score;
    }

    public static int getScoreForLevel(int level)
    {
        if (level == 1)
        {
            return levelOneScore;
        }
        else return levelTwoScore;
    }

    public static void setTimeScoreForLevel(float time, int level) {
        if (level == 1)
        {
            levelOneTimeScore = (int)(10000 / time);
        }
        else levelTwoTimeScore = (int)(10000 / time);
    }

    public static int getTimeScoreForLevel(int level)
    {
        if (level == 1)
        {
            return levelOneTimeScore;
        }
        else return levelTwoTimeScore;
    }
}
