using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Scoreboard : MonoBehaviour
{
    public GameObject score1Text;
    public GameObject score2Text; 
    public GameObject scoreTimeText1;
    public GameObject scoreTimeText2;
    public GameObject totalScore;

    private void Start()
    {
        score1Text.GetComponent<TMPro.TextMeshProUGUI>().text = "Points from Level1: " + ScoreProvider.getScoreForLevel(1);
        score2Text.GetComponent<TMPro.TextMeshProUGUI>().text = "Points from Level2: " + ScoreProvider.getScoreForLevel(2);
        scoreTimeText1.GetComponent<TMPro.TextMeshProUGUI>().text = "Time points from level1: " + ScoreProvider.getTimeScoreForLevel(1);
        scoreTimeText2.GetComponent<TMPro.TextMeshProUGUI>().text = "Time points from level2: " + ScoreProvider.getTimeScoreForLevel(2);
        totalScore.GetComponent<TMPro.TextMeshProUGUI>().text = "Total: " + (ScoreProvider.getScoreForLevel(1) + ScoreProvider.getScoreForLevel(2) + ScoreProvider.getTimeScoreForLevel(1) + ScoreProvider.getTimeScoreForLevel(2));
    }


}
