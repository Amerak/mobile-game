using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager : MonoBehaviour
{
    public Pacman pacman;
    public Transform points;
    public int level = 1;
    public int score { get; private set; }

    private void Start()
    {
        NewGame();
    }


    private void NewGame()
    {
        SetScore(0);
        NewRound();
    }

    private void NewRound() 
    {
        foreach (Transform point in this.points) 
        {
            point.gameObject.SetActive(true);
        }

        ResetState();
    }

    // pokud um�e pacman
    private void ResetState() 
    {
        this.pacman.gameObject.SetActive(true);
    }

    private void SetScore(int score) 
    {
        this.score = score;
    }

    public void PointEaten(Point point) {
        point.gameObject.SetActive(false);
        SetScore(this.score + point.points);
        // Ukon�en� kola
        if (!HasRemainingPoints()) {
            ScoreProvider.setScoreForLevel(this.score, this.level);
            ScoreProvider.setTimeScoreForLevel(Time.timeSinceLevelLoad, this.level);
            if (level == 1) {
                SceneProvider.GoToSecondLevel();
            }
            if (level == 2)
            {
                SceneProvider.GoToScoreboardScene();
            }
        }
    }

    private bool HasRemainingPoints()
    {
        foreach (Transform point in this.points)
        {
            if (point.gameObject.activeSelf) {
                return true;
            }
        }
        return false;
    }
}
