using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MenuFade : MonoBehaviour
{
    private CanvasGroup canvasGroup;

    private void Start()
    {
        canvasGroup = FindObjectOfType<CanvasGroup>();
        canvasGroup.alpha = 1;
    }

    private void Update()
    {
        canvasGroup.alpha = 1 - Time.timeSinceLevelLoad * 0.3f;
    }
}
