using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Preloader : MonoBehaviour
{
    private CanvasGroup fadeGroup;
    private float loadTime = 3f;

    private void Start()
    {
        fadeGroup = FindObjectOfType<CanvasGroup>();

        fadeGroup.alpha = 0;
    }

    private void Update()
    {
        if (Time.time > loadTime && loadTime != 0) {
            fadeGroup.alpha = Time.time - loadTime;

            if (fadeGroup.alpha >= 1) {
                SceneProvider.GoToMenuScene();
            }
        }
    }
}
